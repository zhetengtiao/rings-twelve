package com.linestock.cloudfirecracker;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioAttributes;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    public SoundPool sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AudioAttributes attr = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_GAME) // 设置音效使用场景
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC).build(); // 设置音效的类型
        sp = new SoundPool.Builder().setAudioAttributes(attr) // 设置音效池的属性
                .setMaxStreams(10) // 设置最多可容纳10个音频流
                .build();
        sp.load(this, R.raw.boom, 1);
        sp.load(this, R.raw.firework, 1);
    }

    public void firework_click(View v) throws IOException {
        EditText editText1 =(EditText) findViewById (R.id.number2);
        EditText editText3 =(EditText) findViewById (R.id.editTextNumber2);
        for (int i = 0; i < Integer.parseInt(editText1.getText().toString()); i = i + 1){
            sp.play(2, 1, 1, 0, 0, 1);
            try {
                Thread.sleep( 1000 );
            } catch (Exception e){
                System.exit( 0 ); //退出程序
            }

            sp.play(1, 1, 1, 0, 0, 1);
            try {
                Thread.sleep( Integer.parseInt(editText3.getText().toString()) );
            } catch (Exception e){
                System.exit( 0 ); //退出程序
            }

            try {
                Thread.sleep( 200 );
            } catch (Exception e){
                System.exit( 0 ); //退出程序
            }
        }
    }
    public void fire_click(View v) throws IOException {
        EditText editText1 =(EditText) findViewById (R.id.number);
        EditText editText3 =(EditText) findViewById (R.id.editTextNumber2);
        for (int i = 0; i < Integer.parseInt(editText1.getText().toString()); i = i + 1){
            sp.play(1, 1, 1, 0, 0, 1);
            try {
                Thread.sleep( Integer.parseInt(editText3.getText().toString()) );
            } catch (Exception e){
                System.exit( 0 ); //退出程序
            }
        }
    }
}